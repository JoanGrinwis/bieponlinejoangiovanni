function bookSearch() {
    var search = $('#search').val();
    $('#result').html("");

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=isbn:" + search,
        dataType: "json",
        type: 'GET',
        success: function(data) {
            for (i = 0; i < data.items.length; i++) {
                $('#title').val(data.items[i].volumeInfo.title);
                $('#author').val(data.items[i].volumeInfo.authors[i]);
            }
        }
    });

}
$('#search').change(bookSearch);