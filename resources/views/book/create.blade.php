@extends('layouts.app')

@section('title')
	Nieuw boek toevoegen
@endsection

@section('tools')
<li role="navigation">
	<a onClick="window.history.back()">
		<i class="fa fa-arrow-left"></i>&nbspTerug
	</a>
</li>
@endsection

@section('content')
	<div>
		<p>Scan de ISBN-code, de andere informatie wordt automatisch aangevuld.</p>
	</div>
{!! Form::open(['route' => ['book.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
<div class="form-group">
	<div class="col-sm-6">
		{!! Form::label('isbn', 'ISBN', ['class' => 'control-label']) !!}
		{!! Form::text('isbn', null, ['id' => 'search', 'class' => 'form-control', 'placeholder' => 'ISBN Nummer', 'autofocus' => 'autofocus']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('title', 'Titel', ['class' => 'control-label']) !!}
		{!! Form::text('title', null, ['id' => 'title', 'class' => 'form-control', 'placeholder' => 'De titel hier']) !!}
	</div>
	<div class="col-sm-6">
		{!! Form::label('author', 'Auteur', ['class' => 'control-label']) !!}
		{!! Form::text('author', null, ['id' => 'author', 'class' => 'form-control', 'placeholder' => 'Maak een keuze uit de lijst']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-primary">
			Opslaan
		</button>
	</div>
</div>
{!! Form::close() !!}
@endsection

@section('scripts')
    <script src="/js/bookSearch.js"></script>
@endsection